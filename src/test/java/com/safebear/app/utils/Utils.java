package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;

public class Utils {
    /**
     * Copyright safebear Ltd 2017
     *
     * The Utils page is the one place you go to change any parameters that are used by all the tests,
     * such as the URL of the website.
     *
     * You can also use a 'properties' file if you like for these standard parameters, however it's
     * also useful for standard methods that aren't part of tests but are needed. E.g. we're also
     * using the object to open the website and check it opened correctly.
     *
     */

    WebDriver driver;
    String url;

    public boolean navigateToWebsite(WebDriver driver){
        this.driver = driver;
        url = "http://automate.safebear.co.uk";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }

}
