package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test05_MainPageDropDowns extends BaseTest {
    /**
     * Copyright safebear Ltd 2017
     *
     * This test will pick numbers from the dropdowns and add them together. See Steps below.
     */

    @Test
    public void testDropdowns() {
//        Step 1 Confirm we're on the Welcome Page by checking the page title
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login and confirm we've changed to the Login Page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Log in and confirm we're now on the User Page
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
//        Step 4 Enter two values into the dropdowns ('2' and '2'), click add and ensure that the result is '4'
        assertTrue(userPage.addValuesDropdowns(2,2,4));
    }

}
