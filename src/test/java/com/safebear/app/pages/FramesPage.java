package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FramesPage {

    /**
     * Copyright safebear Ltd 2017
     *
     * Welcome to the Frames Page!
     *
     * WebElements:
     * safebear_logo_image
     *      Element = image
     *      Locator = tag
     *
     * Methods:
     * checkCorrectPage()
     *      Inputs: None
     *      Returns: Boolean True or False
     *      WebElements: None
     *      Info: This method confirms that you are on the correct page by checking the title of the
     *      page starts with the word 'Frame'
     *
     * switchToMainFrame()
     *      Inputs: None
     *      Returns: Nothing currently
     *      WebElements: None
     *      Info: This method switches the focus to the main frame on this page so you can access
     *      the elements within the page
     */


//    Find the Safebear Logo in the main frame
            @FindBy(tagName = "img")
            WebElement safebear_logo_image;

//      Declare a WebDriver object
        WebDriver driver;
//      We need a MainFrame object in order to perform actions within the main frame
        MainFrame mainFrame;

//    This method is run whenever the welcome page object is created (e.g. in BasePage)
        public FramesPage(WebDriver driver){

//        Assign the class instance variable for the WebDriver
            this.driver = driver;

        }
        public boolean checkCorrectPage() {

//          Check that the page has the title 'Frame'
            return driver.getTitle().startsWith("Frame");
        }

        public boolean switchToMainFrame() {

//          Switch to the main frame
            driver.switchTo().frame("main");
//          Create a Main Frame object and pass through the driver
            this.mainFrame = new MainFrame(driver);
//          Click on the logo in the frame
            mainFrame.clickOnSafeBearLogo();
//          Check that we're in the correct frame
            return mainFrame.checkCorrectPage();
        }

}



