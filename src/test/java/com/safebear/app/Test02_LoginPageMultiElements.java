package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class Test02_LoginPageMultiElements extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will enter text into multiple elements identified in a List. See Steps below.
     */

    @Test
    public void testMultiElements() {
//        Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Identify all the input fields and populate them with 'testmulti'
        assertTrue(loginPage.multielements("testmulti"));
    }

}
