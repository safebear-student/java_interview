package com.safebear.app;

import org.junit.Test;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test04_MainPageFrames extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will check the Frames Page functionality. See Steps below.
     */

    @Test
    public void testFrames() {
//        Step 1 Confirm we're on the Welcome Page by checking the page title
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login and confirm we've changed to the Login Page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Log in and confirm we're now on the User Page
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
//        Step 4 Click on the Frames button and switch to frames page
        assertTrue(userPage.clickOnFramesButton(this.framesPage));
//        Step 5 Switch to Main Frame
        assertTrue(framesPage.switchToMainFrame());

    }


}
