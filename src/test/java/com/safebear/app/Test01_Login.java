package com.safebear.app;

import org.junit.Test;
import static junit.framework.TestCase.assertTrue;
/**
 * Created by sstratton on 08/05/17.
 */
public class Test01_Login extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will login to the website. See Steps below.
     */

    @Test
    public void testLogin(){
//        Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login link at top of page and confirm we move to the Login Page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));


    }


}
